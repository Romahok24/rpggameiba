﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebLayer.Hubs
{
    public class TestHub : Hub
    {
        public async Task Find()
        {
            await Clients.Caller.SendAsync("Wait");
        }
    }
}
