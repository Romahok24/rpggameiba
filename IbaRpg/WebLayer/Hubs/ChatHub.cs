﻿using BusinessLogicLayer.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebLayer.Hubs
{
    public class ChatHub : Hub
    {
        IChatService _service;

        public ChatHub(IChatService service)
        {
            _service = service;
        }

        public override async Task OnConnectedAsync()
        {
            await Clients.Caller.SendAsync("GetId");
            await base.OnConnectedAsync();
        }

        public async Task Send(string message)
        {
            await this.Clients.All.SendAsync("Send", message);
        }

        public async Task TakeId(string id)
        {
            var user = await _service.GetUserByClaimsPrincipal(Context.User);
            await this.Clients.Caller.SendAsync("Send", user.UserName);
        }
    }

}
