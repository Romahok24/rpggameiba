﻿using BusinessLogicLayer.BusinessModel;
using BusinessLogicLayer.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebLayer.Hubs
{
    public class PvpBattleHub : Hub
    {
        IPvpBattleService _service;

        public PvpBattleHub(IPvpBattleService service)
        {
            _service = service;
        }

        public async Task Find()
        {
            SimpleBattle battle = await _service.Find(Context.User, Context.ConnectionId);

            if (battle == null)
            {
                await Clients.Caller.SendAsync("Wait");
                return;
            }
            
            await Clients.Client(battle.Player1.Id).SendAsync("GetId", battle.Player1.Id);
            await Clients.Client(battle.Player2.Id).SendAsync("GetId", battle.Player2.Id);

            await Groups.AddToGroupAsync(battle.Player1.Id, battle.Id);
            await Groups.AddToGroupAsync(battle.Player2.Id, battle.Id);

            await Clients.Group(battle.Id).SendAsync("Start", battle);
        }

        public async Task MakeTurn(string playerId, string x, string y)
        {
            int prevX, prevY;
            Player player;
            Battle game;//.Replace(" / auto", "")

            (game, player, prevX, prevY) = _service.MakeTurn(playerId, Convert.ToInt32(x[0].ToString()), Convert.ToInt32(y[0].ToString()));

            if (game == null)
                return;

            await Clients.Group(game.Id).SendAsync("MakeTurn", player, prevX, prevY);

            if (!game.WhoseTurn.Equals(player))
            {             
                await Clients.Group(game.Id).SendAsync("UpdateTurn", game.WhoseTurn.Id);
                await Clients.Client(game.WhoseTurn.Id).SendAsync("GetAvailableTurns", game.GetAvailableTurns(game.WhoseTurn));
            }
        }

        public async Task Attack(string playerId)
        {
            Player player, opponent;
            Battle game;

            (game, player, opponent) = _service.Attack(playerId);
            await Clients.Group(game.Id).SendAsync("GetDamage", opponent);

            if (!game.IsOver)
            {
                if (!game.WhoseTurn.Equals(player))                    
                    await Clients.Group(game.Id).SendAsync("UpdateTurn", game.WhoseTurn.Id);

                await Clients.Client(game.WhoseTurn.Id).SendAsync("GetAvailableTurns", game.GetAvailableTurns(game.WhoseTurn));

            }
            else
            {
                await Clients.Group(game.Id).SendAsync("Win", player.Hero.Name);
                _service.RemoveGame(game.Id);
            }
        }

        public async Task GetSet(int lol)
        {
            await Clients.Caller.SendAsync("Start", lol);
        }
    }
}
