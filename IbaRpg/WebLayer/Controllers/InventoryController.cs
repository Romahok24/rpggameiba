﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BusinessLogicLayer.Interfaces;

namespace WebLayer.Controllers
{
    public class InventoryController : Controller
    {
        IInventoryService _service;

        public InventoryController(IInventoryService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}