﻿using System;
using System.Threading.Tasks;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebLayer.Controllers
{
    [Authorize]
    public class PersonageController : Controller
    {
        IPersonageService _service;

        public PersonageController(IPersonageService service)
        {
            _service = service;
        }

        // GET: Personage
        public async Task<IActionResult> Index()
        {
            return View(await _service.GetUsersHero(HttpContext.User));
        }

        // GET: Personage/Details/5
        public ActionResult Details(int? id)
        {
            return View(_service.GetHeroById((int)id));
        }

        // GET: Personage/Create
        public ActionResult Create()
        {
            ViewBag.Races = new SelectList(_service.GetRaces(), "Id", "Name");
            return View();
        }

        // POST: Personage/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(HeroDto hero)
        {
            try
            {
                await _service.Create(hero, HttpContext.User);

                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                return View(nameof(Index));
            }
        }

        // GET: Personage/Delete/5
        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(int? id)
        {
            if (id != null)
            {
                var hero = _service.GetHeroById((int)id);
                if (hero != null)
                    return View(hero);
            }
            return NotFound();
        }

        // POST: Personage/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id != null)
            {
                await _service.Delete((int)id);
                return RedirectToAction("Index");
            }
            return NotFound();

        }

        public async Task<IActionResult> Play(int? id)
        {
            var hero = await _service.Play(id, HttpContext.User);
            return RedirectToAction("Index", "Game", hero.LocationId);
        }
    }
}