﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace WebLayer.Controllers
{
    public class GameController : Controller
    {
        IGameService _service;

        public GameController(IGameService service)
        {
            _service = service;
        }

        public async Task<IActionResult> Index(int? locationId)
        {
            var game = await _service.GetGame(locationId, HttpContext.User);
            return View(game);
        }

        public IActionResult TestView()
        {
            return View();
        }

        public IActionResult Battle()
        {
            return View("PvPBattle");
        }
    }
}