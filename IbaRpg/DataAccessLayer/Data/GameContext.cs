﻿using DataAccessLayer.Models;
using DataAccessLayer.Models.Heroes;
using DataAccessLayer.Models.Items;
using DataAccessLayer.Models.Items.Armors;
using DataAccessLayer.Models.Items.Weapons;
using DataAccessLayer.Models.Levels;
using DataAccessLayer.Models.NPC;
using DataAccessLayer.Models.Races;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Data
{
    /*public class GameContext : DbContext
    {
        public DbSet<Hero> Heroes { get; set; }
        public DbSet<Item> Items { get; set; }

        public DbSet<Level> Levels { get; set; }
        public DbSet<HeroLevel> HeroLevels { get; set; }
        public DbSet<ItemLevel> ItemLevels { get; set; }

        public DbSet<Race> Races { get; set; }

        public DbSet<Armor> Armors { get; set; }
        public DbSet<Weapon> Weapons { get; set; }

        public DbSet<BodyArmor> BodyArmors { get; set; }
        public DbSet<HelmetArmor> HelmetArmors { get; set; }
        public DbSet<BootsArmor> BootsArmors { get; set; }
        public DbSet<GauntletsArmor> GauntletsArmors { get; set; }

        public DbSet<MagicWeapon> MagicWeapons { get; set; }
        public DbSet<SmallWeapon> SmallWeapons { get; set; }
        public DbSet<ShieldAndWeapon> ShieldAndWeapons { get; set; }
        public DbSet<TwoHandWeapon> TwoHandWeapons { get; set; }

        public DbSet<Location> Locations { get; set; }

        public DbSet<BaseNPC> BaseNPCs { get; set; }
        public DbSet<FriendlyNPC> FriendlyNPCs { get; set; }
        public DbSet<EnemyNPC> EnemyNPCs { get; set; }

        DbSet<PlayerAndItem> InventoriesAndItems { get; set; }

        public GameContext(DbContextOptions<GameContext> options) : base(options)
        { 
        }

        public GameContext() : base()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TestGameDb3;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }
    }*/
}
