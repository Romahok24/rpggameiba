﻿using DataAccessLayer.Models;
using DataAccessLayer.Models.Items.Armors;
using DataAccessLayer.Models.Items.Weapons;
using DataAccessLayer.Models.Levels;
using DataAccessLayer.Models.Races;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessLayer.Data
{
    public class DbInitializer
    {
        public static void Initialize()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ItemLevel level = new ItemLevel();

            if (!context.Levels.Any())
            {
                List<HeroLevel> hLevels = new List<HeroLevel>();
                int exp = 900;
                double q = 1.15;

                hLevels.Add(new HeroLevel { Value = 1, NeedExperience = 0 });
                for (int i = 2; i <= 100; i++)
                {
                    hLevels.Add(new HeroLevel {Value = i, NeedExperience = exp });
                    exp = (int)(exp * q);
                }
                context.HeroLevels.AddRange(hLevels);

                List<ItemLevel> iLevels = new List<ItemLevel>();
                for (int i = 1; i <= 20; i++)
                    iLevels.Add(new ItemLevel { Value = (i - 1) * 5 });

                context.ItemLevels.AddRange(iLevels);
                level = iLevels.SingleOrDefault(o => o.Value == 0);
            }

            if (!context.Races.Any())
            {
                context.AddRange(new Race[]
                {
                    new Race {Name = "Зомби"},
                    new Race {Name = "Лич"},
                    new Race {Name = "Вампир"},
                    new Race {Name = "Снежный Эльф"},
                    new Race {Name = "Высокий Эльф"},
                    new Race {Name = "Темный Эльф"},
                    new Race {Name = "Гоблин"},
                    new Race {Name = "Рептилоид"},
                    new Race {Name = "Кошак"}
                });
            }

            if (!context.Items.Any())
            {
                context.BodyArmors.Add(new BodyArmor { Name = "Туника", Weight = 1, Level = level, LevelId = level.Id, Cost = 0 });
                context.HelmetArmors.Add(new HelmetArmor { Name = "Шапка", Weight = 1, Level = level, LevelId = level.Id, Cost = 0 });
                context.BootsArmors.Add(new BootsArmor { Name = "Кожанные перчатки", Weight = 1, Level = level, LevelId = level.Id, Cost = 0 });
                context.GauntletsArmors.Add(new GauntletsArmor { Name = "Кожанные сапоги", Weight = 1, Level = level, LevelId = level.Id, Cost = 0 });
                context.ShieldAndWeapons.Add(new ShieldAndWeapon { Name = "Железный меч и щит", Weight = 1, Level = level, LevelId = level.Id, Cost = 0, Damage = 10, BodyProtection = 10 });
                context.TwoHandWeapons.Add(new TwoHandWeapon { Name = "Железные кинжалы", Weight = 1, Level = level, LevelId = level.Id, Cost = 0, Damage = 20 });
                context.MagicWeapons.Add(new MagicWeapon { Name = "Посох малой магии", Weight = 1, Level = level, LevelId = level.Id, Cost = 0, Damage = 5, ManaCost = 10, Range = 6, RangeDamage = 10 });
            }

            if (!context.Locations.Any())
            {
                context.Locations.AddRange(new Location[]{
                    new Location{Name = "Секелаг", PrevLocation = 2, NextLocation = 3, Path = "/img/irstlock.jpg"},
                    new Location{Name = "Альмарен", PrevLocation = 1, NextLocation = 3, Path = "/img/second.jpg"},
                    new Location{Name = "Темпест", PrevLocation = 2, NextLocation = 1, Path = "/img/tempest.jpg"}
                });
            }

            context.SaveChanges();
        }
    }
}
