﻿using DataAccessLayer.Models.Heroes;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace DataAccessLayer.Models.Users
{
    public class User : IdentityUser
    {
        public IEnumerable<Hero> Heroes { get; set; }
    }
}
