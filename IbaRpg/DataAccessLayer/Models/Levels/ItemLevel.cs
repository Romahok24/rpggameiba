﻿using DataAccessLayer.Models.Items;
using System.Collections.Generic;

namespace DataAccessLayer.Models.Levels
{
    public class ItemLevel : Level
    {
        public int MaxLevel { get; set; }
        public List<Item> Items { get; set; }
    }
}
