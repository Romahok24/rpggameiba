﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Models.Levels
{
    public class Level
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public string Discriminator { get; set; }
    }
}
