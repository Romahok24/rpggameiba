﻿using DataAccessLayer.Models.Heroes;
using System.Collections.Generic;

namespace DataAccessLayer.Models.Levels
{
    public class HeroLevel : Level
    {
        public int NeedExperience { get; set; }
        public List<Hero> Heroes { get; set; }
    }
}
