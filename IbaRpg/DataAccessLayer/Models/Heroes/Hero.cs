﻿using DataAccessLayer.Models.Items;
using DataAccessLayer.Models.Items.Armors;
using DataAccessLayer.Models.Items.Weapons;
using DataAccessLayer.Models.Levels;
using DataAccessLayer.Models.Races;
using DataAccessLayer.Models.Users;
using System.Collections.Generic;
using System.ComponentModel;

namespace DataAccessLayer.Models.Heroes
{
    public class Hero
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Health { get; set; }

        public int MovePoints { get; set; }

        public int Force { get; set; }

        public int Agility { get; set; }

        public int Intelligence { get; set; }

        public int Weight { get; set; }

        public int Experience { get; set; }

        public int Money { get; set; }

        public int? ArmorId { get; set; }
        public BodyArmor Armor { get; set; }

        public int? HelmetId { get; set; }
        public HelmetArmor Helmet { get; set; }

        public int? GauntletsId { get; set; }
        public GauntletsArmor Gauntlets { get; set; }

        public int? BootsId { get; set; }
        public BootsArmor Boots { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public int? LevelId { get; set; }
        public Level Level { get; set; }

        public int? WeaponId { get; set; }
        public Weapon Weapon { get; set; }

        public int? RaceId { get; set; }
        [DisplayName("Раса")]
        public Race Race { get; set; }

        public int? LocationId { get; set; }
        public Location Location { get; set; }

        public bool IsSelectToPlay { get; set; } = false;

        public bool IsBlocked { get; set; } = false;

        public IEnumerable<PlayerAndItem> PlayerAndItems { get; set; }
    }
}