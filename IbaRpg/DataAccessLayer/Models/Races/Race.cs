﻿using DataAccessLayer.Models.Heroes;
using System.Collections.Generic;

namespace DataAccessLayer.Models.Races
{
    public class Race
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Hero> Heroes { get; set; }
    }
}
