﻿using System.ComponentModel;

namespace DataAccessLayer.Models.NPC
{
    public class EnemyNPC : BaseNPC
    {
        [DisplayName("Здоровье")]
        public int Health { get; set; }

        [DisplayName("Сила")]
        public int Stamina { get; set; }

        [DisplayName("Мана")]
        public int Mana { get; set; }

        [DisplayName("Урон")]
        public int Damage { get; set; }
    }
}
