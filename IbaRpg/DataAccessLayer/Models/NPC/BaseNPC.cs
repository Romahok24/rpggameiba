﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Models.NPC
{
    public class BaseNPC
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? LocationId { get; set; }

        public Location Location { get; set; }

        public int X { get; set; }

        public int Y { get; set; }
    }
}
