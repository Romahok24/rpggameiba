﻿using System.Collections.Generic;

namespace DataAccessLayer.Models.NPC
{
    public class FriendlyNPC : BaseNPC
    {
        public IEnumerable<Quest> Quests { get; set; } = new List<Quest>();
    }
}
