﻿using DataAccessLayer.Models.Levels;
using System;
using System.Collections.Generic;

namespace DataAccessLayer.Models.Items
{
    public class Item
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int Weight { get; set; }
        public int? LevelId { get; set; }
        public ItemLevel Level { get; set; }
        public int Cost { get; set; }
        public bool CanEquip { get; set; }
        public bool CanUse { get; set; }
        public ICollection<PlayerAndItem> InventoryAndItems { get; set; }
        public string Discriminator { get; set; }
    }
}
