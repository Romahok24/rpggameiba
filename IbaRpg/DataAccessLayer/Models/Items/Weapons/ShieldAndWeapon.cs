﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Models.Items.Weapons
{
    public class ShieldAndWeapon : MeleeWeapon
    {
        public int BodyProtection { get; set; }
    }
}
