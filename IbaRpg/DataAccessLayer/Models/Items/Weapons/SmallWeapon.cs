﻿namespace DataAccessLayer.Models.Items.Weapons
{
    public class SmallWeapon : RangeWeapon
    {
        public int StaminaCost { get; set; }
    }
}
