﻿namespace DataAccessLayer.Models.Items.Weapons
{
    public class RangeWeapon : Weapon
    {
        public int Range { get; set; }
        public int RangeDamage { get; set; }
    }
}
