﻿using DataAccessLayer.Models.Heroes;
using System.Collections.Generic;

namespace DataAccessLayer.Models.Items.Weapons
{
    public class Weapon : Item
    {
        public int Damage { get; set; }
        public ICollection<Hero> Heroes { get; set; }
    }
}
