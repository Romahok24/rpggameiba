﻿namespace DataAccessLayer.Models.Items.Weapons
{
    public class TwoHandWeapon : MeleeWeapon
    {
        public int StaminaCost { get; set; }
    }
}
