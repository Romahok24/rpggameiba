﻿using DataAccessLayer.Models.Heroes;

namespace DataAccessLayer.Models.Items
{
    public class PlayerAndItem
    {
        public int Id { get; set; }
        public int? HeroId { get; set; }
        public Hero Hero { get; set; }
        public int? ItemId { get; set; }
        public Item Item { get; set; }
    }
}
