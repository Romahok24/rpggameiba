﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Models.Items
{
    public class Inventory
    {
        [Key]
        [ForeignKey("Hero")]
        public int Id { get; set; }
        public ICollection<PlayerAndItem> InventoryAndItems { get; set; }
    }
}
