﻿using DataAccessLayer.Models.Heroes;
using System.Collections.Generic;

namespace DataAccessLayer.Models.Items.Armors
{
    public class BodyArmor : Armor
    {
        public int BodyProtection { get; set; } = 1;
        public ICollection<Hero> Heroes { get; set; }
    }
}
