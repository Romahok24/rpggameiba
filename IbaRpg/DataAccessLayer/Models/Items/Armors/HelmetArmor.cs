﻿using DataAccessLayer.Models.Heroes;
using System.Collections.Generic;

namespace DataAccessLayer.Models.Items.Armors
{
    public class HelmetArmor : Armor
    {
        public int HeadProtection { get; set; } = 1;
        public ICollection<Hero> Heroes { get; set; }
    }
}
