﻿using DataAccessLayer.Models.Heroes;
using System.Collections.Generic;

namespace DataAccessLayer.Models.Items.Armors
{
    public class GauntletsArmor : Armor
    {
        public int HandProtection { get; set; } = 1;
        public ICollection<Hero> Heroes { get; set; }
    }
}
