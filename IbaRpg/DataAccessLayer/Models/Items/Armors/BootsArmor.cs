﻿using DataAccessLayer.Models.Heroes;
using System.Collections.Generic;

namespace DataAccessLayer.Models.Items.Armors
{
    public class BootsArmor : Armor
    {
        public int LegProtection { get; set; } = 1;
        public ICollection<Hero> Heroes { get; set; }
    }
}
