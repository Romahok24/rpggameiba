﻿using DataAccessLayer.Models.Heroes;
using System.Collections.Generic;

namespace DataAccessLayer.Models
{
    public class Location
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int? NextLocation { get; set; }

        public int? PrevLocation { get; set; }

        public string Path { get; set; }

        public string MapPath { get; set; }

        public IEnumerable<Hero> Heroes { get; set; } = new List<Hero>();
    }
}
