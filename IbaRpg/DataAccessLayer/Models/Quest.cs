﻿using DataAccessLayer.Models.NPC;

namespace DataAccessLayer.Models
{
    public class Quest
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? NpcId { get; set; }

        public FriendlyNPC NPC { get; set; }
    }
}
