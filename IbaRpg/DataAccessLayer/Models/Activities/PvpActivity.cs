﻿using DataAccessLayer.Models.Heroes;

namespace DataAccessLayer.Models.Activities
{
    class PvpActivity : Activity
    {
        public Hero Enemy { get; set; }
    }
}
