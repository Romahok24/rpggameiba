﻿using DataAccessLayer.Models.NPC;

namespace DataAccessLayer.Models.Activities
{
    class PveActivity : Activity
    {
        public EnemyNPC Enemy { get; set; }
    }
}
