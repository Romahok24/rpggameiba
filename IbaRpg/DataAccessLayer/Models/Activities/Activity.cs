﻿using DataAccessLayer.Enums;
using DataAccessLayer.Models.Heroes;
using System;

namespace DataAccessLayer.Models.Activities
{
    class Activity
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public Hero Hero { get; set; }
        public BatlleResult Result { get; set; }
    }
}
