﻿using DataAccessLayer.Data;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using DataAccessLayer.Models.Heroes;
using DataAccessLayer.Models.Items;
using DataAccessLayer.Models.Items.Armors;
using DataAccessLayer.Models.Items.Weapons;
using DataAccessLayer.Models.Levels;
using DataAccessLayer.Models.NPC;
using DataAccessLayer.Models.Races;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.AspNetCore.Identity;
using DataAccessLayer.Models.Users;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        #region Variables
        private ApplicationDbContext _context;
        //private ApplicationDbContext _userContext;

        private IRepository<Hero> _heroRepository;
        private IRepository<Item> _itemRepository;

        private IRepository<Level> _levelRepository;
        private IRepository<HeroLevel> _heroLevelRepository;
        private IRepository<ItemLevel> _itemLevelRepository;

        private IRepository<Race> _raceRepository;

        private IRepository<Armor> _armorRepository;
        private IRepository<Weapon> _weaponRepository;

        private IRepository<BodyArmor> _bodyArmorRepository;
        private IRepository<HelmetArmor> _helmetArmorRepository;
        private IRepository<BootsArmor> _bootsArmorRepository;
        private IRepository<GauntletsArmor> _gauntletsArmorRepository;

        private IRepository<MagicWeapon> _magicWeaponRepository;
        private IRepository<SmallWeapon> _smallWeaponRepository;
        private IRepository<ShieldAndWeapon> _shieldWeaponRepository;
        private IRepository<TwoHandWeapon> _twoHandWeaponRepository;

        private IRepository<Location> _locationRepository;

        private IRepository<BaseNPC> _baseNPCRepository;
        private IRepository<FriendlyNPC> _friendlyNPCRepository;
        private IRepository<EnemyNPC> _enemyNPCRepository;

        private IRepository<PlayerAndItem> _inventoryAndItemsRepository;

        private IRepository<User> _userRepository;

        private UserManager<User> _users;
        private RoleManager<Role> _roles;
        private SignInManager<User> _signInManager;

        private bool disposed = false;
        #endregion

        public EFUnitOfWork(//GameContext context, 
                            ApplicationDbContext userContext, 
                            UserManager<User> userManager, 
                            RoleManager<Role> roleManager,
                            SignInManager<User> signInManager)
        {
            _context = userContext;
            //_userContext = userContext;

            _users = userManager;
            _roles = roleManager;
            _signInManager = signInManager;
        }

        #region Properties
        public IRepository<Hero> Heroes => _heroRepository ?? (_heroRepository = new EFRepository<Hero>(_context));

        public IRepository<Item> Items => _itemRepository ?? (_itemRepository = new EFRepository<Item>(_context));

        public IRepository<Level> Levels => _levelRepository ?? (_levelRepository = new EFRepository<Level>(_context));

        public IRepository<HeroLevel> HeroLevels => _heroLevelRepository ?? (_heroLevelRepository = new EFRepository<HeroLevel>(_context));

        public IRepository<ItemLevel> ItemLevels => _itemLevelRepository ?? (_itemLevelRepository = new EFRepository<ItemLevel>(_context));

        public IRepository<Race> Races => _raceRepository ?? (_raceRepository = new EFRepository<Race>(_context));

        public IRepository<Armor> Armors => _armorRepository ?? (_armorRepository = new EFRepository<Armor>(_context));

        public IRepository<Weapon> Weapons => _weaponRepository ?? (_weaponRepository = new EFRepository<Weapon>(_context));

        public IRepository<BodyArmor> BodyArmors => _bodyArmorRepository ?? (_bodyArmorRepository = new EFRepository<BodyArmor>(_context));

        public IRepository<HelmetArmor> HelmetArmors => _helmetArmorRepository ?? (_helmetArmorRepository = new EFRepository<HelmetArmor>(_context));

        public IRepository<BootsArmor> BootsArmors => _bootsArmorRepository ?? (_bootsArmorRepository = new EFRepository<BootsArmor>(_context));

        public IRepository<GauntletsArmor> GauntletsArmors => _gauntletsArmorRepository ?? (_gauntletsArmorRepository = new EFRepository<GauntletsArmor>(_context));

        public IRepository<MagicWeapon> MagicWeapons => _magicWeaponRepository ?? (_magicWeaponRepository = new EFRepository<MagicWeapon>(_context));

        public IRepository<SmallWeapon> SmallWeapons => _smallWeaponRepository ?? (_smallWeaponRepository = new EFRepository<SmallWeapon>(_context));

        public IRepository<ShieldAndWeapon> ShieldAndWeapons => _shieldWeaponRepository ?? (_shieldWeaponRepository = new EFRepository<ShieldAndWeapon>(_context));

        public IRepository<TwoHandWeapon> TwoHandWeapons => _twoHandWeaponRepository ?? (_twoHandWeaponRepository = new EFRepository<TwoHandWeapon>(_context));

        public IRepository<Location> Locations => _locationRepository ?? (_locationRepository = new EFRepository<Location>(_context));

        public IRepository<BaseNPC> BaseNPCs => _baseNPCRepository ?? (_baseNPCRepository = new EFRepository<BaseNPC>(_context));

        public IRepository<FriendlyNPC> FriendlyNPCs => _friendlyNPCRepository ?? (_friendlyNPCRepository = new EFRepository<FriendlyNPC>(_context));

        public IRepository<EnemyNPC> EnemyNPCs => _enemyNPCRepository ?? (_enemyNPCRepository = new EFRepository<EnemyNPC>(_context));

        public IRepository<User> Users => _userRepository ?? (_userRepository = new EFRepository<User>(_context));

        public UserManager<User> UserManager => _users;

        public RoleManager<Role> RoleManager => _roles;

        public SignInManager<User> SignInManager => _signInManager;

        public IRepository<PlayerAndItem> PlayersAndItems => _inventoryAndItemsRepository ?? (_inventoryAndItemsRepository = new EFRepository<PlayerAndItem>(_context));
        #endregion

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
