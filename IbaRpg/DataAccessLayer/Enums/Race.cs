﻿namespace DataAccessLayer.Enums
{
    public enum Race
    {
        Zombie = 1,
        SnowElf,
        HighElf,
        DarkElf,
        Goblin,
        Reptiloid,
        Koshak,
        Lich,
        Vampire
    }
}
