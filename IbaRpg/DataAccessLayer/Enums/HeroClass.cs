﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum HeroClass
    {
        Warrior = 1,
        Mage,
        Assassin
    }
}
