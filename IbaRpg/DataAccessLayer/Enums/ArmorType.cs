﻿using System.ComponentModel;

namespace DataAccessLayer.Enums
{
    public enum ArmorType
    {
        [Description("Тело")]
        BodyArmor,

        [Description("Сапоги")]
        BootsArmor,

        [Description("Шлем")]
        HelmetArmor,

        [Description("Перчатки")]
        GauntletsArmor
    }
}
