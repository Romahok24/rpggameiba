﻿namespace DataAccessLayer.Enums
{
    public enum StartArmor
    {
        Body = 1,
        Helmet,
        Gauntlets,
        Boots  
    }
}
