﻿using DataAccessLayer.Models;
using DataAccessLayer.Models.Heroes;
using DataAccessLayer.Models.Items;
using DataAccessLayer.Models.Items.Armors;
using DataAccessLayer.Models.Items.Weapons;
using DataAccessLayer.Models.Levels;
using DataAccessLayer.Models.NPC;
using DataAccessLayer.Models.Races;
using DataAccessLayer.Models.Users;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Hero> Heroes { get; }
        IRepository<Item> Items { get; }

        IRepository<Level> Levels { get; }
        IRepository<HeroLevel> HeroLevels { get; }
        IRepository<ItemLevel> ItemLevels { get; }

        IRepository<Race> Races { get; }

        IRepository<Armor> Armors { get; }
        IRepository<Weapon> Weapons { get; }

        IRepository<BodyArmor> BodyArmors { get; }
        IRepository<HelmetArmor> HelmetArmors { get; }
        IRepository<BootsArmor> BootsArmors { get; }
        IRepository<GauntletsArmor> GauntletsArmors { get; }

        IRepository<MagicWeapon> MagicWeapons { get; }
        IRepository<SmallWeapon> SmallWeapons { get; }
        IRepository<ShieldAndWeapon> ShieldAndWeapons { get; }
        IRepository<TwoHandWeapon> TwoHandWeapons { get; }

        IRepository<Location> Locations { get; }

        IRepository<BaseNPC> BaseNPCs { get; }
        IRepository<FriendlyNPC> FriendlyNPCs { get; }
        IRepository<EnemyNPC> EnemyNPCs { get; }

        IRepository<PlayerAndItem> PlayersAndItems { get; }
        
        IRepository<User> Users { get; }

        UserManager<User> UserManager { get; } 
        RoleManager<Role> RoleManager { get; }
        SignInManager<User> SignInManager { get; }

        void Save();
        Task<int> SaveAsync();
    }
}
