﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    class SmallWeaponDto : RangeWeaponDto
    {
        public int StaminaCost { get; set; }
    }
}
