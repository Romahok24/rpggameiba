﻿using System.Collections.Generic;

namespace BusinessLogicLayer.DTO
{
    public class InventoryDto
    {
        public int Id { get; set; }
        public ICollection<PlayerAndItemDto> InventoryAndItems { get; set; }
    }
}