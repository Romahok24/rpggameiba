﻿using System.Collections.Generic;

namespace BusinessLogicLayer.DTO
{
    public class HelmetArmorDto : ArmorDto
    {
        public int HeadProtection { get; set; }
        public ICollection<HeroDto> Heroes { get; set; }
    }
}