﻿using System.Collections.Generic;
using System.ComponentModel;

namespace BusinessLogicLayer.DTO
{
    public class HeroDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Health { get; set; } = 100;

        public int MovePoints { get; set; } = 5;

        public int Force { get; set; } = 4;

        public int Agility { get; set; } = 4;

        public int Intelligence { get; set; } = 4;

        public int Weight { get; set; } = 50;

        public int Experience { get; set; }

        public int Money { get; set; }

        public int? ArmorId { get; set; }
        public BodyArmorDto Armor { get; set; }

        public int? HelmetId { get; set; }
        public HelmetArmorDto Helmet { get; set; }

        public int? GauntletsId { get; set; }
        public GauntletsArmorDto Gauntlets { get; set; }

        public int? BootsId { get; set; }
        public BootsArmorDto Boots { get; set; }

        public string UserId { get; set; }

        [DisplayName("Пользователь")]
        public UserDto User { get; set; }

        public int? LevelId { get; set; }

        [DisplayName("Уровень")]
        public LevelDto Level { get; set; }

        public int? WeaponId { get; set; }

        [DisplayName("Оружие")]
        public WeaponDto Weapon { get; set; }

        public int? RaceId { get; set; }

        [DisplayName("Раса")]
        public RaceDto Race { get; set; }

        public bool IsSelectToPlay { get; set; } = false;

        public int? LocationId { get; set; }

        public LocationDto Location { get; set; }

        [DisplayName("Блокировка")]
        public bool IsBlocked { get; set; } = false;

        public IEnumerable<PlayerAndItemDto> PlayerAndItems { get; set; } = new List<PlayerAndItemDto>();
    }
}
