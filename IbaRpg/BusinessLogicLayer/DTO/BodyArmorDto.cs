﻿using System.Collections.Generic;

namespace BusinessLogicLayer.DTO
{
    public class BodyArmorDto : ArmorDto
    {
        public int BodyProtection { get; set; }
        public ICollection<HeroDto> Heroes { get; set; }
    }
}