﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class EnemyNPCDto : BaseNPCDto
    {
        [DisplayName("Здоровье")]
        public int Health { get; set; }

        [DisplayName("Сила")]
        public int Stamina { get; set; }

        [DisplayName("Мана")]
        public int Mana { get; set; }

        [DisplayName("Урон")]
        public int Damage { get; set; }
    }
}
