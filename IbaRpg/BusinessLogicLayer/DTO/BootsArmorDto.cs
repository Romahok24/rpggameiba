﻿using System.Collections.Generic;

namespace BusinessLogicLayer.DTO
{
    public class BootsArmorDto : ArmorDto
    {
        public int LegProtection { get; set; } = 1;
        public ICollection<HeroDto> Heroes { get; set; }
    }
}