﻿namespace BusinessLogicLayer.DTO
{
    public class QuestDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? NpcId { get; set; }

        public FriendlyNPCDto NPC { get; set; }
    }
}