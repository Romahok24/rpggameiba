﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class ArmorDto : ItemDto
    {
        public bool CanEquip { get; set; }
    }
}
