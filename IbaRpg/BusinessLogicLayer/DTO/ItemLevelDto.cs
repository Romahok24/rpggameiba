﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class ItemLevelDto : LevelDto
    {
        public int MaxLevel { get; set; }
        public List<ItemDto> Items { get; set; }
    }

}
