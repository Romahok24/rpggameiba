﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    class ShieldAndWeaponDto : MeleeWeaponDto
    {
        public int BodyProtection { get; set; }
    }
}
