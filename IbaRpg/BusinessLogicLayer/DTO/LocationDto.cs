﻿using System.Collections.Generic;

namespace BusinessLogicLayer.DTO
{
    public class LocationDto
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int? NextLocation { get; set; }

        public int? PrevLocation { get; set; }

        public string Path { get; set; }

        public IEnumerable<HeroDto> Heroes { get; set; }
    }
}