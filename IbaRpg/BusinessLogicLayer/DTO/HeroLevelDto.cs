﻿using System.Collections.Generic;

namespace BusinessLogicLayer.DTO
{
    class HeroLevelDto : LevelDto
    {
        public int NeedExperience { get; set; }
        public List<HeroDto> Heroes { get; set; }
    }
}
