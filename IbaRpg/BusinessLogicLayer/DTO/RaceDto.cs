﻿using System.Collections.Generic;

namespace BusinessLogicLayer.DTO
{
    public class RaceDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<HeroDto> Heroes { get; set; }
    }
}