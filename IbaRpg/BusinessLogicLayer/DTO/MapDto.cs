﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class MapDto
    {
        public int X { get; set; }
        public int Y { get; set; }
        public String Name { get; set; }
        public bool IsClicable { get; set; }
    }
}
