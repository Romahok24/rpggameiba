﻿using System.Collections.Generic;

namespace BusinessLogicLayer.DTO
{
    public class GauntletsArmorDto : ArmorDto
    {
        public int HandProtection { get; set; }
        public ICollection<HeroDto> Heroes { get; set; }
    }
}