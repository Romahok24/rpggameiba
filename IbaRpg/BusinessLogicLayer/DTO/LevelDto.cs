﻿namespace BusinessLogicLayer.DTO
{
    public class LevelDto
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public string Discriminator { get; set; }
    }
}