﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class ItemDto
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int Weight { get; set; }
        public int? LevelId { get; set; }
        public bool CanEquip { get; set; }
        public bool CanUse { get; set; }
        public ItemLevelDto Level { get; set; }
        public int Cost { get; set; }
        public ICollection<PlayerAndItemDto> InventoryAndItems { get; set; }
        public string Discriminator { get; set; }
    }
}
