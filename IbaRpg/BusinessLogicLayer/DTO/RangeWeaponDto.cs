﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    class RangeWeaponDto : WeaponDto
    {
        public int Range { get; set; }
        public int RangeDamage { get; set; }
    }
}
