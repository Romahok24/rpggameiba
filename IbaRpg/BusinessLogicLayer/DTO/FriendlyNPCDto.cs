﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class FriendlyNPCDto : BaseNPCDto
    {
        public IEnumerable<QuestDto> Quests { get; set; }
    }
}
