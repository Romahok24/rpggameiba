﻿namespace BusinessLogicLayer.DTO
{
    public class PlayerAndItemDto
    {
        public int Id { get; set; }
        public int? HeroId { get; set; }
        public HeroDto Hero { get; set; }
        public int? ItemId { get; set; }
        public ItemDto Item { get; set; }
    }
}