﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class GameDto
    {
        public UserDto User { get; set; }
        public HeroDto Hero { get; set; }
        public LocationDto CurrentLocation { get; set; }
        public List<MapDto> Map { get; set; }
        public IEnumerable<LocationDto> NextLocations { get; set; }
        public IEnumerable<HeroDto> HeroesOnMap { get; set; } = new List<HeroDto>();
    }
}
