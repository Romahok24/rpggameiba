﻿using System.Collections.Generic;

namespace BusinessLogicLayer.DTO
{
    public class WeaponDto : ItemDto
    {
        public bool CanEquip { get; set; }
        public int Damage { get; set; }
        public ICollection<HeroDto> Heroes { get; set; }
    }
}