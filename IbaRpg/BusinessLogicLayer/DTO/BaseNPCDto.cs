﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class BaseNPCDto
    {
        public int Id { get; set; }

        public int Name { get; set; }

        public int? LocationId { get; set; }

        public LocationDto Location { get; set; }

        public int X { get; set; }

        public int Y { get; set; }
    }
}
