﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    class MagicWeaponDto : RangeWeaponDto
    {
        public int ManaCost { get; set; }
    }
}
