﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    class TwoHandWeaponDto : MeleeWeaponDto
    {
        public int StaminaCost { get; set; }
    }
}
