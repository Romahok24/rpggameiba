﻿using AutoMapper;
using BusinessLogicLayer.BusinessModel;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Utility.Profile;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class GameService : IGameService
    {
        IUnitOfWork Database { get; set; }
        IMapper Mapper { get; set; }

        public GameService(IUnitOfWork uow)
        {
            Database = uow;

            Mapper = new MapperConfiguration(cfg => cfg.AddProfile(new ApplicationProfile())).CreateMapper();
        }

        public EnemyNPCDto GetEnemy(int? npcId)
        {
            throw new NotImplementedException();
        }

        public FriendlyNPCDto GetFriend(int? npcId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get game object for user.
        /// </summary>
        /// <param name="locationId">Location id</param>
        /// <param name="userClaims">User</param>
        /// <returns></returns>
        public async Task<GameDto> GetGame(int? locationId, ClaimsPrincipal userClaims)
        {
            var user = await Database.UserManager.GetUserAsync(userClaims);

            var hero = Database.Heroes.FirstOrDefault(h => h.UserId.Equals(user.Id) && h.IsSelectToPlay);

            if (locationId == null)
                locationId = hero.LocationId;
            
            var current = Database.Locations.FirstOrDefault(l => l.Id == locationId);

            var nextLocations = Database.Locations.Get(l => l.NextLocation == locationId || l.PrevLocation == locationId);
            var heroes = Database.Heroes.Get(h => h.IsSelectToPlay && h.LocationId == locationId && h.Id != hero.Id);

            GameDto game = new GameDto
            {
                User = Mapper.Map<UserDto>(user),
                Hero = Mapper.Map<HeroDto>(hero),
                CurrentLocation = Mapper.Map<LocationDto>(current),
                Map = MapLoader.LoadMap(Database, current.Path).ToList(),
                NextLocations = Mapper.Map<IEnumerable<LocationDto>>(nextLocations),
                HeroesOnMap = Mapper.Map<IEnumerable<HeroDto>>(heroes)
            };

            return game;
        }
    }
}
