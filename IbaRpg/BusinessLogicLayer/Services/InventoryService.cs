﻿using AutoMapper;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Infrastructure;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Utility.Profile;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models.Heroes;
using DataAccessLayer.Models.Items;
using DataAccessLayer.Models.Items.Armors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class InventoryService : IInventoryService
    {
        IUnitOfWork Database { get; set; }
        IMapper Mapper { get; set; }

        public InventoryService(IUnitOfWork uow, ClaimsPrincipal user)
        {
            Database = uow;
            Mapper = new MapperConfiguration(cfg => cfg.AddProfile(new ApplicationProfile())).CreateMapper();
        }

        /// <summary>
        /// Find item by id and equip on the player
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="userClaims"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Equip(int itemId, ClaimsPrincipal userClaims)
        {
            var user = await Database.UserManager.GetUserAsync(userClaims);
            var hero = Database.Heroes.FirstOrDefault(h => h.UserId.Equals(user.Id) && h.IsSelectToPlay);
            var item = Database.PlayersAndItems.GetWithInclude(i => i.ItemId == itemId && hero.Id == i.HeroId, o => o.Item)
                                               .FirstOrDefault();

            if (item.Item.CanEquip)
            {
                PlayerAndItemDto dto = new PlayerAndItemDto() { HeroId = hero.Id};

                if (item.Item.Discriminator.Equals(typeof(HelmetArmor).Name))
                {
                    dto.ItemId = (int)hero.HelmetId;
                    hero.HelmetId = itemId;                
                }

                if (item.Item.Discriminator.Equals(typeof(BodyArmor).Name))
                {
                    dto.ItemId = (int)hero.ArmorId;
                    hero.ArmorId = itemId;
                }

                if (item.Item.Discriminator.Equals(typeof(GauntletsArmor).Name))
                {
                    dto.ItemId = (int)hero.GauntletsId;
                    hero.GauntletsId = itemId;
                }

                if (item.Item.Discriminator.Equals(typeof(BootsArmor).Name))
                {
                    dto.ItemId = (int)hero.BootsId;
                    hero.BootsId = itemId;
                }

                Database.Heroes.Update(Mapper.Map<Hero>(hero));
                Database.PlayersAndItems.Remove(item);
                Database.PlayersAndItems.Create(Mapper.Map<PlayerAndItem>(dto));
                Database.Save();
                return new OperationDetails(true, "", "");
            }

            return new OperationDetails(false, "", "");
        }

        /// <summary>
        /// Get all player items.
        /// </summary>
        /// <param name="userClaim">User</param>
        /// <returns></returns>
        public async Task<IEnumerable<ItemDto>> GetItems(ClaimsPrincipal userClaim)
        {
            var user = await Database.UserManager.GetUserAsync(userClaim);
            var hero = Database.Heroes.FirstOrDefault(h => h.IsSelectToPlay && h.UserId.Equals(user));
            var items = Database.PlayersAndItems
                                .GetWithInclude(o => o.HeroId == hero.Id, i => i.Item)
                                .Select(o => o.Item).GroupBy(g => g.Name);

  
            return Mapper.Map<IEnumerable<ItemDto>>(items);
        }

        /// <summary>
        /// Take off item form player
        /// </summary>
        /// <param name="id">Item id</param>
        /// <param name="userClaims">User</param>
        /// <returns></returns>
        public async Task<OperationDetails> TakeOff(int id, ClaimsPrincipal userClaims)
        {
            try
            {
                var user = await Database.UserManager.GetUserAsync(userClaims);
                var hero = Database.Heroes.FirstOrDefault(h => h.UserId.Equals(user.Id) && h.IsSelectToPlay);

                int? itemId = null;

                if (hero.ArmorId == id)
                {
                    itemId = hero.ArmorId;
                    hero.ArmorId = null;
                }

                if (hero.HelmetId == id)
                {
                    itemId = hero.HelmetId;
                    hero.HelmetId = null;
                }

                if (hero.GauntletsId == id)
                {
                    itemId = hero.GauntletsId;
                    hero.GauntletsId = null;
                }

                if (hero.BootsId == id)
                {
                    itemId = hero.BootsId;
                    hero.BootsId = null;
                }

                Database.PlayersAndItems.Create(new PlayerAndItem { HeroId = hero.Id, ItemId = itemId });
                await Database.SaveAsync();
                return new OperationDetails(true, "Предмет снят", "");
            }
            catch(Exception ex)
            {
                return new OperationDetails(false, ex.Message, "");
            }
        }

        /// <summary>
        /// Throw item from inventory. 
        /// </summary>
        /// <param name="id">Item id</param>
        /// <param name="userClaim"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Throw(int id, ClaimsPrincipal userClaim)
        {
            var user = await Database.UserManager.GetUserAsync(userClaim);
            var hero = Database.Heroes.FirstOrDefault(h => h.IsSelectToPlay && h.UserId.Equals(user));
            var item = Database.PlayersAndItems
                               .Get(o => o.HeroId == hero.Id && o.ItemId == id)
                               .SingleOrDefault();

            if (item == null)
                return new OperationDetails(false, "Ошибка.", "");

            Database.PlayersAndItems.Remove(item);
            Database.Save();
            return new OperationDetails(true, "Предмет выброшен.", "");
        }

        public Task<OperationDetails> Use(int id)
        {
            throw new NotImplementedException();
        }
    }
}
