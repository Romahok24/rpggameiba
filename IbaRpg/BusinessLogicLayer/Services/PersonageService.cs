﻿using AutoMapper;
using BusinessLogicLayer.Builders;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Infrastructure;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Utility.Profile;
using DataAccessLayer.Enums;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using DataAccessLayer.Models.Heroes;
using DataAccessLayer.Models.Items.Weapons;
using DataAccessLayer.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class PersonageService : IPersonageService
    {
        IUnitOfWork Database { get; set; }
        IMapper Mapper { get; set; }

        public PersonageService(IUnitOfWork uow)
        {
            Database = uow;

            Mapper = new MapperConfiguration(cfg => cfg.AddProfile(new ApplicationProfile())).CreateMapper();
        }

        /// <summary>
        /// Delete hero.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Delete(HeroDto item)
        {
            try
            {
                var hero = Database.Heroes.FindById(item.Id);

                if (hero != null)
                {
                    Database.Heroes.Remove(hero);
                    await Database.SaveAsync();

                    return new OperationDetails(true, "Персонаж успешно удален", "");
                }

                return new OperationDetails(false, "Не удалось найти данного персонажа", "");
            }
            catch (Exception ex) 
            {
                return new OperationDetails(false, ex.Message, "");
            }
        }

        /// <summary>
        /// Admin part. Move personage from one user to another. 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="personage"></param>
        /// <returns></returns>
        public async Task<OperationDetails> MovePersonage(UserDto from, UserDto to, HeroDto personage)
        {
            try
            {
                var hero = Database.Heroes.FindById(personage.Id);

                if (hero != null)
                {
                    var userTo = Mapper.Map<User>(to);
                    hero.User = userTo;
                    hero.UserId = hero.User.Id;

                    var user = Mapper.Map<User>(from);
                    user.Heroes.ToList().Remove(hero);

                    await Database.SaveAsync();

                    return new OperationDetails(true, "Персонаж успешно перемещён", "");
                }

                return new OperationDetails(false, "Не удалось найти данного персонажа", "");
            }
            catch(Exception ex)
            {
                return new OperationDetails(false, ex.Message, "");
            }
        }

        /// <summary>
        /// Get all heroes.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<HeroDto> Read()
        {
            var list = Database.Heroes.GetWithInclude(u => u.User,
                                                      lvl => lvl.Level,
                                                      l => l.Location,
                                                      r => r.Race,
                                                      w => w.Weapon,
                                                      a => a.Armor,
                                                      h => h.Helmet,
                                                      g => g.Gauntlets,
                                                      b => b.Boots);

            return Mapper.Map<IEnumerable<Hero>, IEnumerable<HeroDto>>(list);
        }

        /// <summary>
        /// Update hero
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Update(HeroDto item)
        {
            try
            {
                var hero = Database.Heroes.FindById(item.Id);

                if (hero != null)
                {
                    hero = Mapper.Map<Hero>(item);
                    Database.Heroes.Update(hero);
                    await Database.SaveAsync();

                    return new OperationDetails(true, "Персонаж успешно обновлен", "");
                }

                return new OperationDetails(false, "Персонаж не найден", "");
            }
            catch(Exception ex)
            {
                return new OperationDetails(false, ex.Message, "");
            }
        }

        /// <summary>
        /// Create hero.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Create(HeroDto item)
        {
            try
            {
                var race = (Race)Enum.GetValues(typeof(Race)).GetValue((int)item.RaceId - 1);

                var hero = Mapper.Map<HeroDto, Hero>(item);
                var heroDto = GetHero(hero.User, hero.Name, race);

                hero = Mapper.Map<Hero>(heroDto);

                Database.Heroes.Create(hero);
                await Database.SaveAsync();

                return new OperationDetails(true, "Персонаж успешно создан", "");
            }
            catch (Exception ex)
            {
                return new OperationDetails(false, ex.Message, "");
            }
        }


        /// <summary>
        /// Create hero.
        /// </summary>
        /// <param name="hero"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Create(HeroDto hero, UserDto user)
        {
            hero.UserId = user.Id;
            hero.User = user;

            return await Create(hero);
        }

        /// <summary>
        /// Create hero.
        /// </summary>
        /// <param name="hero"></param>
        /// <param name="userClaims"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Create(HeroDto hero, ClaimsPrincipal userClaims)
        {
            var id = Database.UserManager.GetUserId(userClaims);
            var user = Database.Users.FirstOrDefault(u => u.Id.Equals(id));

            var userDto = Mapper.Map<UserDto>(user);

            return await Create(hero, userDto);
        }

        /// <summary>
        /// Get hero for game.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userClaims"></param>
        /// <returns></returns>
        public async Task<HeroDto> Play(int? id, ClaimsPrincipal userClaims)
        {
            var user = await Database.UserManager.GetUserAsync(userClaims);
            var heroes = Database.Heroes.Get(h => h.UserId.Equals(user.Id));
            var falsePlayHero = heroes.Where(h => h.IsSelectToPlay);

            if (falsePlayHero != null) 
            {
                foreach (var player in falsePlayHero)
                {
                    player.IsSelectToPlay = false;
                    Database.Heroes.Update(player);
                }
            }

            Hero hero = heroes.FirstOrDefault(h => h.Id == id);

            if (hero != null)
            {
                hero.IsSelectToPlay = true;
                Database.Heroes.Update(hero);
                await Database.SaveAsync();
            }
            else
                throw new Exception("Герой не найден");

            return Mapper.Map<HeroDto>(hero);
        }

        /// <summary>
        /// Get all heroes by user id.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<HeroDto> GetUsersHero(string userId)
        {
            var heroes = Database.Heroes.GetWithInclude(o => o.UserId.Equals(userId), r => r.Race, l => l.Level);
            return Mapper.Map<IEnumerable<HeroDto>>(heroes);
        }

        /// <summary>
        /// Exit from game.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Exit(ClaimsPrincipal user)
        {
            var exitUser = await Database.UserManager.GetUserAsync(user);
            var hero = Database.Heroes
                               .Get(o => o.IsSelectToPlay && o.UserId == exitUser.Id)
                               .FirstOrDefault();

            if (hero != null) 
            {
                hero.IsSelectToPlay = false;
                Database.Heroes.Update(hero);
                await Database.SaveAsync();

                return new OperationDetails(true, "Выход успешно выполнен", "");
            }

            return new OperationDetails(false, "Ошибка", "");
        }

        /// <summary>
        /// Get all races.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RaceDto> GetRaces()
        {
            return Mapper.Map<List<RaceDto>>(Database.Races.Get());
        }

        /// <summary>
        /// Get start armor.
        /// </summary>
        /// <param name="hero"></param>
        private void GetStartArmor(HeroDto hero)
        {
            hero.ArmorId = (int)StartArmor.Body;
            hero.HelmetId = (int)StartArmor.Helmet;
            hero.BootsId = (int)StartArmor.Boots;
            hero.GauntletsId = (int)StartArmor.Gauntlets;
        }

        /// <summary>
        /// Get start level.
        /// </summary>
        /// <returns></returns>
        private LevelDto GetLevel()
        {
            var level = Database.HeroLevels.FirstOrDefault(o => o.Id == 1);
            return Mapper.Map<HeroLevelDto>(level);
        }

        /// <summary>
        /// Get start weapon.
        /// </summary>
        /// <param name="heroClass"></param>
        /// <returns></returns>
        private WeaponDto GetWeapon(HeroClass heroClass)
        {
            Weapon weapon = null;

            switch (heroClass)
            {
                case HeroClass.Warrior:
                    weapon = Database.ShieldAndWeapons
                                     .Get(o => o.Discriminator.Equals(nameof(ShieldAndWeapon)))
                                     .FirstOrDefault();
                    break;
                case HeroClass.Assassin:
                    weapon = Database.TwoHandWeapons
                                     .Get(o => o.Discriminator
                                     .Equals(nameof(TwoHandWeapon))).FirstOrDefault();
                    break;
                default:
                    weapon = Database.MagicWeapons
                                     .Get(o => o.Discriminator.Equals(nameof(MagicWeapon)))
                                     .FirstOrDefault();
                    break;
            }

            return Mapper.Map<WeaponDto>(weapon);
        }

        /// <summary>
        /// Get start location.
        /// </summary>
        /// <param name="race"></param>
        /// <returns></returns>
        private LocationDto GetLocation(Race race)
        {
            Location location = null;

            if (race == Race.Lich || race == Race.Zombie || race == Race.Vampire)
                location = Database.Locations.FirstOrDefault(o => o.Id == (int)Fractions.Deadland);

            if (race == Race.DarkElf || race == Race.HighElf || race == Race.SnowElf)
                location = Database.Locations.FirstOrDefault(o => o.Id == (int)Fractions.Almaren);

            if (race == Race.Goblin || race == Race.Reptiloid || race == Race.Koshak)
                location = Database.Locations.FirstOrDefault(o => o.Id == (int)Fractions.Tempest);

            return Mapper.Map<LocationDto>(location);
        }

        /// <summary>
        /// Get start race.
        /// </summary>
        /// <param name="race"></param>
        /// <returns></returns>
        private RaceDto GetRace(Race race)
        {
            var raceModel = Database.Races.FirstOrDefault(o => o.Id == (int)race);
            return Mapper.Map<RaceDto>(raceModel);
        }

        /// <summary>
        /// Gete hero.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="heroName"></param>
        /// <param name="race"></param>
        /// <returns></returns>
        private HeroDto GetHero(User user, string heroName, Race race)
        {
            HeroClass heroClass = HeroClass.Warrior;
            if (race == Race.Lich || race == Race.HighElf || race == Race.Reptiloid)
                heroClass = HeroClass.Mage;

            if (race == Race.DarkElf || race == Race.Koshak || Race.Vampire == race)
                heroClass = HeroClass.Assassin;

            var level = GetLevel();
            var weapon = GetWeapon(heroClass);
            var location = GetLocation(race);
            var heroRace = GetRace(race);

            HeroDto hero = new HeroDto
            {
                UserId = user.Id,
                Name = heroName,
                LevelId = level.Id,
                WeaponId = weapon.Id,
                LocationId = location.Id,
                RaceId = heroRace.Id
            };

            GetStartArmor(hero);
            SkillBuilder.GetStartStats(hero, race);

            return hero;
        }

        /// <summary>
        /// Get all user heroes.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<IEnumerable<HeroDto>> GetUsersHero(ClaimsPrincipal user)
        {
            var item = await Database.UserManager.GetUserAsync(user);

            return GetUsersHero(item.Id);
        }

        /// <summary>
        /// Get hero by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HeroDto GetHeroById(int id)
        {
            var hero = Database.Heroes
                               .GetWithInclude(p => p.Id == id, r => r.Race, l => l.Level, o => o.Location)
                               .FirstOrDefault();

            return Mapper.Map<HeroDto>(hero);
        }

        /// <summary>
        /// Delete hero.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Delete(int id)
        {
            try
            {
                var hero = Database.Heroes.FindById(id);

                Database.Heroes.Remove(hero);
                await Database.SaveAsync();

                return new OperationDetails(false, "Герой удален!", "");
            }
            catch (Exception ex)
            {
                return new OperationDetails(false, ex.Message, "");
            }
        }

    }
}
