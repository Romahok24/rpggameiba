﻿using AutoMapper;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Infrastructure;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Utility.Profile;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    /// <summary>
    /// Admin part. Managing user accounts.
    /// </summary>
    public class AccountService : IAccountService
    {
        IUnitOfWork Database { get; set; }
        IMapper Mapper { get; set; }

        public AccountService(IUnitOfWork uow)
        {
            Database = uow;
            Mapper = new MapperConfiguration(cfg => cfg.AddProfile(new ApplicationProfile())).CreateMapper();
        }

        /// <summary>
        /// Create new user.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Create(UserDto item)
        {
            User user = await Database.UserManager.FindByEmailAsync(item.Email);

            if (user == null)
            {
                user = new User { Email = item.Email, UserName = item.UserName };

                var result = await Database.UserManager.CreateAsync(user, item.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault().ToString(), "");

                await Database.UserManager.AddToRoleAsync(user, item.Role);

                Database.Save();

                return new OperationDetails(true, "Регистрация успешно пройдена", "");
            }
            else
            {
                return new OperationDetails(false, "Пользователь с таким логином уже существует", "Email");
            }
        }

        /// <summary>
        /// Delete user.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Delete(UserDto item)
        {
            User user = await Database.UserManager.FindByIdAsync(item.Id);

            if(user != null)
            {
                var result = await Database.UserManager.DeleteAsync(user);

                if(result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault().ToString(), "");

                return new OperationDetails(true, "Удаление прошло успешно", "");
            }

            return new OperationDetails(false, "Пользователь не найден", "");
        }

        /// <summary>
        /// Show user details.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<UserDto> Details(UserDto item)
        {
            var user = await Database.UserManager.FindByIdAsync(item.Id);

            if(user == null)
                throw new ValidationException("Пользователь не найден", "");

            return Mapper.Map<User, UserDto>(user);
        }

        /// <summary>
        /// Get all users.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserDto> Read()
        {
            var list = Database.UserManager.Users.ToList();
            return Mapper.Map<IEnumerable<User>, IEnumerable<UserDto>>(list);
        }

        /// <summary>
        /// Update user.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Update(UserDto item)
        {
            User user = await Database.UserManager.FindByIdAsync(item.Id);

            if (user != null)
            {
                user = Mapper.Map<UserDto, User>(item);
                var result = await Database.UserManager.UpdateAsync(user);

                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault().ToString(), "");

                return new OperationDetails(true, "Обновление прошло успешно", "");
            }

            return new OperationDetails(false, "Пользователь не найден", "");
        }
    }
}
