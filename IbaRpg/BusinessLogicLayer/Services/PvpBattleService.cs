﻿using AutoMapper;
using BusinessLogicLayer.BusinessModel;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Utility.Profile;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class PvpBattleService : IPvpBattleService
    {
        IUnitOfWork Database { get; set; }
        IMapper Mapper { get; set; }

        private static ConcurrentDictionary<string, Player> players =
            new ConcurrentDictionary<string, Player>(StringComparer.OrdinalIgnoreCase);

        private static readonly ConcurrentDictionary<string, Battle> games =
            new ConcurrentDictionary<string, Battle>(StringComparer.OrdinalIgnoreCase);

        private static readonly ConcurrentDictionary<string, BattleField> fields =
           new ConcurrentDictionary<string, BattleField>(StringComparer.OrdinalIgnoreCase);


        public PvpBattleService(IUnitOfWork uow)
        {
            Database = uow;

            Mapper = new MapperConfiguration(cfg => cfg.AddProfile(new ApplicationProfile())).CreateMapper();
        }

        /// <summary>
        /// Attack opponent.
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public (Battle, Player, Player) Attack(string playerId)
        {
            Player player = GetPlayer(playerId);
            Player opponent;
            Battle game = GetGame(player, out opponent);

            if (game == null || !game.WhoseTurn.Equals(player))
                return (null, null, null);

            game.MakeHit();

            return (game, player, opponent);
        }

        /// <summary>
        /// Find opponent for battle.
        /// </summary>
        /// <param name="claims"></param>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        public async Task<SimpleBattle> Find(ClaimsPrincipal claims, string connectionId)
        {
            var user = await Database.UserManager.GetUserAsync(claims);
            var hero = Database.Heroes.GetWithInclude(r => r.Race).FirstOrDefault(h => h.UserId.Equals(user.Id) && h.IsSelectToPlay);

            var player = new Player(Mapper.Map<HeroDto>(hero))
            {
                Id = connectionId,
                Looking = true,
            };

            players.TryAdd(connectionId, player);
            Player opponent = GetPlayers().FirstOrDefault(o => !o.Id.Equals(connectionId));

            if (opponent == null)
                return null;

            player.Looking = false;
            opponent.Looking = false;

            Battle newGame = CreateBattle(player, opponent);
           
            return GetSimpleBattle(newGame);
        }

        public Task GetSet(int lol)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Make turn.
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public (Battle, Player, int, int) MakeTurn(string playerId, int x, int y)
        {
            Player player = GetPlayer(playerId);
            Player opponent;
            Battle game = GetGame(player, out opponent);

            if (game == null || !game.WhoseTurn.Equals(player))
                return (null, player, 0, 0);

            var prevX = player.Coordinate.X;
            var prevY = player.Coordinate.Y;
            var turn = game.MakeTurn(x, y);

            if (!turn)
                return (null, player, 0, 0);

            return (game, player, prevX, prevY);
        }

        /// <summary>
        /// Create pvp battle.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="opponent"></param>
        /// <returns></returns>
        private Battle CreateBattle(Player player, Player opponent)
        {
            Battle game = new Battle(player, opponent) { Id = Guid.NewGuid().ToString() };
            games[game.Id] = game;

            player.GameId = game.Id;
            opponent.GameId = game.Id;

            BattleField field = new BattleField() { Id = Guid.NewGuid().ToString(), GameId = game.Id };
            fields[field.Id] = field;
            game.Field = field;

            player.Coordinate.X = 0;
            player.Coordinate.Y = 5;

            opponent.Coordinate.X = 11;
            opponent.Coordinate.Y = 5;

            field.Field[player.Coordinate.Y][player.Coordinate.X] = 1;
            field.Field[opponent.Coordinate.Y][opponent.Coordinate.X] = 1;

            return game;
        }

        /// <summary>
        /// Get player by id.
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        private Player GetPlayer(string playerId)
        {
            Player foundPlayer;
            if (!players.TryGetValue(playerId, out foundPlayer))
            {
                return null;
            }

            return foundPlayer;
        }

        /// <summary>
        /// Get game and player's opponent.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="opponent"></param>
        /// <returns></returns>
        private Battle GetGame(Player player, out Player opponent)
        {
            opponent = null;
            Battle foundGame = games.Values.FirstOrDefault(g => g.Id.Equals(player.GameId));

            if (foundGame == null)
            {
                return null;
            }

            opponent = (player.Id.Equals(foundGame.Player1.Id)) ?
                foundGame.Player2 :
                foundGame.Player1;

            return foundGame;
        }

        /// <summary>
        /// Remove battle when game over.
        /// </summary>
        /// <param name="gameId"></param>
        public void RemoveGame(string gameId)
        {
            Battle foundGame;

            if (!games.TryRemove(gameId, out foundGame))
            {
                throw new InvalidOperationException("Game not found.");
            }

            Player foundPlayer;
            players.TryRemove(foundGame.Player1.Id, out foundPlayer);
            players.TryRemove(foundGame.Player2.Id, out foundPlayer);
        }

        private List<Player> GetPlayers()
        {
            List<Player> list = new List<Player>();

            foreach (var item in players)
                if (item.Value.Looking)
                    list.Add(item.Value);

            return list;
        }

        private SimpleBattle GetSimpleBattle(Battle battle)
        {
            SimplePlayer player1 = new SimplePlayer
            {
                Id = battle.Player1.Id,
                Name = battle.Player1.Hero.Name,
                Coordinate = battle.Player1.Coordinate,
                ImgPath = battle.Player1.ImgPath,
                Health = battle.Player1.Health
            };

            SimplePlayer player2 = new SimplePlayer
            {
                Id = battle.Player2.Id,
                Name = battle.Player2.Hero.Name,
                Coordinate = battle.Player2.Coordinate,
                ImgPath = battle.Player2.ImgPath,
                Health = battle.Player2.Health
            };

            return new SimpleBattle { Id = battle.Id,  Player1 = player1, Player2 = player2, Field = battle.Field };
        } 
    }
}
