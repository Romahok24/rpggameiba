﻿using AutoMapper;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Utility.Profile;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class ChatService : IChatService
    {
        IUnitOfWork Database { get; set; }
        IMapper Mapper { get; set; }

        public ChatService(IUnitOfWork uow)
        {
            Database = uow;

            Mapper = new MapperConfiguration(cfg => cfg.AddProfile(new ApplicationProfile())).CreateMapper();
        }

        /// <summary>
        /// Get user by id.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        public async Task<UserDto> GetUserById(string id)
        {
            var user = await Database.UserManager.FindByIdAsync(id);

            return Mapper.Map<UserDto>(user);
        }

        /// <summary>
        /// Get user by claims.
        /// </summary>
        /// <param name="claims"></param>
        /// <returns></returns>
        public async Task<UserDto> GetUserByClaimsPrincipal(ClaimsPrincipal claims)
        {
            var user = await Database.UserManager.GetUserAsync(claims);

            return Mapper.Map<UserDto>(user);
        }
    }
}
