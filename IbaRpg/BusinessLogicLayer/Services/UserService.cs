﻿using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Infrastructure;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models.Users;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }

        public UserService(IUnitOfWork uow)
        {
            Database = uow;
        }

        /// <summary>
        /// Register new user.
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Register(UserDto userDto)
        {
            User user = await Database.UserManager.FindByEmailAsync(userDto.Email);

            if(user == null)
            {
                user = new User { Email = userDto.Email, UserName = userDto.UserName };

                var result = await Database.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault().ToString(), "");

                //await Database.UserManager.AddToRoleAsync(user, userDto.Role);

                Database.Save();

                await Database.SignInManager.SignInAsync(user, false);
                return new OperationDetails(true, "Регистрация успешно пройдена", "");
            }
            else
            {
                return new OperationDetails(false, "Пользователь с таким логином уже существует", "Email");
            }
        }

        /// <summary>
        /// Authenticate user.
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        public async Task<SignInResult> Authenticate(UserDto userDto)
        {
            return await Database.SignInManager
                                 .PasswordSignInAsync(userDto.UserName, userDto.Password, userDto.RememberMe, false);
        }

        /// <summary>
        /// Logout user.
        /// </summary>
        /// <returns></returns>
        public async Task Logout()
        {
            await Database.SignInManager.SignOutAsync();
        }
    }
}
