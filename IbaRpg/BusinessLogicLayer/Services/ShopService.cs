﻿using AutoMapper;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Infrastructure;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Utility.Profile;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models.Heroes;
using DataAccessLayer.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class ShopService : IShopService
    {
        IUnitOfWork Database { get; set; }
        IMapper Mapper { get; set; }

        public ShopService(IUnitOfWork uow)
        {
            Database = uow;

            Mapper = new MapperConfiguration(cfg => cfg.AddProfile(new ApplicationProfile())).CreateMapper();
        }

        /// <summary>
        /// Buy item.
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="userClaims"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Buy(int? itemId, ClaimsPrincipal userClaims)
        {
            var user = await Database.UserManager.GetUserAsync(userClaims);
            var hero = Database.Heroes.FirstOrDefault(h => h.UserId.Equals(user.Id) && h.IsSelectToPlay);
            var item = Database.Items.FirstOrDefault(i => i.Id == itemId);

            int heroWeight = Database.PlayersAndItems.GetWithInclude(pi => pi.HeroId == hero.Id, i => i.Item)
                                                     .Select(o => o.Item)
                                                     .Sum(s => s.Weight);

            if(heroWeight + item.Weight <= hero.Weight)
            {
                if (hero.Money >= item.Cost)
                {
                    var playerAndItem = new PlayerAndItemDto
                    {
                        HeroId = hero.Id,
                        ItemId = item.Id
                    };

                    hero.Money -= item.Cost;

                    Database.PlayersAndItems.Create(Mapper.Map<PlayerAndItem>(playerAndItem));
                    Database.Heroes.Update(hero);

                    await Database.SaveAsync();
                    return new OperationDetails(true, "Предмет успешно приобретен", "");
                }

                return new OperationDetails(false, "Недостаточно денег", "");
            }

            return new OperationDetails(false, "Недостаточно места в инвенторе", "");
        }

        /// <summary>
        /// Sell item.
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="userClaims"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Sell(int? itemId, ClaimsPrincipal userClaims)
        {
            var user = await Database.UserManager.GetUserAsync(userClaims);
            var hero = Database.Heroes.FirstOrDefault(h => h.UserId.Equals(user.Id) && h.IsSelectToPlay);
            var item = Database.Items.FirstOrDefault(i => i.Id == itemId);
            var playerAndItem = Database.PlayersAndItems.FirstOrDefault(pl => pl.HeroId == hero.Id && pl.ItemId == itemId);

            if(playerAndItem != null)
            {
                hero.Money += (int)(item.Cost * 40 / 100);

                Database.Heroes.Update(hero);
                Database.PlayersAndItems.Remove(playerAndItem);

                await Database.SaveAsync();

                return new OperationDetails(true, "Предмет продан", "");
            }

            return new OperationDetails(false, "", "");
        }
    }
}
