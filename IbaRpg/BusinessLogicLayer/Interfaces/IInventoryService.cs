﻿using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Infrastructure;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IInventoryService
    {
        Task<OperationDetails> Use(int id);
        Task<OperationDetails> Equip(int itemId, ClaimsPrincipal userClaims);
        Task<OperationDetails> Throw(int id, ClaimsPrincipal userClaim);
        Task<OperationDetails> TakeOff(int id, ClaimsPrincipal userClaims);
        Task<IEnumerable<ItemDto>> GetItems(ClaimsPrincipal userClaim);

    }
}
