﻿using BusinessLogicLayer.DTO;
using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;

namespace BusinessLogicLayer.Interfaces
{
    public interface IStatisticService
    {
        /// <summary>
        /// Игровая активность за заданный отрезок времени.
        /// </summary>
        /// <param name="from">С какой даты</param>
        /// <param name="to">По какую дату</param>
        /// <param name="user">Пользователь</param>
        /// <returns></returns>
        IEnumerable<ActivityDto> GetGameActivity(DateTime from, DateTime to, UserDto user);

        /// <summary>
        /// Завершённые сражения с заданным результатом и периодом времени.
        /// </summary>
        /// <param name="result">Результат сражения</param>
        /// <param name="from">С какой даты</param>
        /// <param name="to">По какую дату</param>
        /// <returns></returns>
        IEnumerable<ActivityDto> GetPersonagesActions(BatlleResult result, DateTime? from = null, DateTime? to = null);

        /// <summary>
        /// Время в сражениях с заданным NPC, у заданного пользователя.
        /// </summary>
        /// <param name="enemy">Пользователь</param>
        /// <param name="user"></param>
        /// <returns></returns>
        IEnumerable<ActivityDto> GetBattleTime(EnemyNPCDto enemy, UserDto user);

    }
}
