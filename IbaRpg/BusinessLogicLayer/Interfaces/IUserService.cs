﻿using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IUserService
    {
        Task<OperationDetails> Register(UserDto userDto);
        Task<SignInResult> Authenticate(UserDto userDto);
        Task Logout();
    }
}
