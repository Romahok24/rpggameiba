﻿using BusinessLogicLayer.DTO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IGameService
    {
        Task<GameDto> GetGame(int? locationId, ClaimsPrincipal userClaims);
        FriendlyNPCDto GetFriend(int? npcId);
        EnemyNPCDto GetEnemy(int? npcId);
    }
}
