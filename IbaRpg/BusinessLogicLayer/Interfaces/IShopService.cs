﻿using BusinessLogicLayer.Infrastructure;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IShopService
    {
        Task<OperationDetails> Buy(int? itemId, ClaimsPrincipal userClaims);
        Task<OperationDetails> Sell(int? itemId, ClaimsPrincipal userClaims);
    }
}
