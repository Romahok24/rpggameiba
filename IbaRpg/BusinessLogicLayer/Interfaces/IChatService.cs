﻿using BusinessLogicLayer.DTO;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IChatService
    {
        Task<UserDto> GetUserById(string id);
        Task<UserDto> GetUserByClaimsPrincipal(ClaimsPrincipal claims);
    }
}
