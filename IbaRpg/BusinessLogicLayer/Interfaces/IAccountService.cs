﻿using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IAccountService
    {
        Task<OperationDetails> Create(UserDto item);
        IEnumerable<UserDto> Read();
        Task<OperationDetails> Update(UserDto item);
        Task<OperationDetails> Delete(UserDto item);
        Task<UserDto> Details(UserDto item);
    }
}
