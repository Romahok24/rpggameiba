﻿using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Infrastructure;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IPersonageService
    {
        Task<OperationDetails> MovePersonage(UserDto from, UserDto to, HeroDto personage);
        Task<OperationDetails> Create(HeroDto hero, UserDto user);
        Task<OperationDetails> Create(HeroDto hero, ClaimsPrincipal user);
        IEnumerable<HeroDto> GetUsersHero(string userId);
        Task<IEnumerable<HeroDto>> GetUsersHero(ClaimsPrincipal user);
        Task<HeroDto> Play(int? id, ClaimsPrincipal userClaims);
        Task<OperationDetails> Exit(ClaimsPrincipal user);
        IEnumerable<RaceDto> GetRaces();
        HeroDto GetHeroById(int id);
        Task<OperationDetails> Delete(int id);
    }
}
