﻿using BusinessLogicLayer.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface ICrudService<T>
    {
        Task<OperationDetails> Create(T item);
        IEnumerable<T> Read();
        Task<OperationDetails> Update(T item);
        Task<OperationDetails> Delete(T item);

    }
}
