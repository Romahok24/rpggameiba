﻿using BusinessLogicLayer.BusinessModel;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IPvpBattleService
    {
        Task<SimpleBattle> Find(ClaimsPrincipal claims, string connectionId);
        (Battle, Player, int, int) MakeTurn(string playerId, int x, int y);
        (Battle, Player, Player) Attack(string playerId);
        Task GetSet(int lol);
        void RemoveGame(string gameId);
    }
}
