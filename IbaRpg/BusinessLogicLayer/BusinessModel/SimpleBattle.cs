﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.BusinessModel
{
    public class SimpleBattle
    {
        public string Id { get; set; }
        public SimplePlayer Player1 { get; set; }
        public SimplePlayer Player2 { get; set; }
        public BattleField Field { get; set; }

        public SimpleBattle() { }
    }
}
