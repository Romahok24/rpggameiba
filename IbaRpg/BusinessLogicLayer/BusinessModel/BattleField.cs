﻿using System;
using System.IO;

namespace BusinessLogicLayer.BusinessModel
{
    public class BattleField
    {
        private int[][] field = new int[Height][];

        public const int Height = 10;
        public const int Width = 12;

        public string Id { get; set; }
        public string GameId { get; set; }
        public int[][] Field 
        { 
            get { return field; }
            set { field = value; } 
        }

        public BattleField() 
        {
            for (int i = 0; i < Height; i++)
                field[i] = new int[12];
        }

        public int[][] LoadFromFile(string path)
        {
            using (StreamReader sr = new StreamReader("testmap.txt"))
            {
                string[] stringFromFile = sr.ReadToEnd().Split(new char[] { '\n', ',' }, StringSplitOptions.RemoveEmptyEntries);

                int k = 0;
                for(int i = 0; i < Height; i++)
                {
                    for(int j = 0; j < Width; j++)
                    {
                        field[i][j] = Convert.ToInt32(stringFromFile[k++]);
                    }
                }
            }

            return field;
        }
    }
}
