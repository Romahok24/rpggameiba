﻿using BusinessLogicLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.BusinessModel
{
    public class DamageCalculator
    {
        public const int DamageActionPoint = 3;

        public DamageCalculator() { }

        public void CalculateDamage(Player whoHits, Player whoGetsHit)
        {
            if (whoHits.Points < DamageActionPoint)
                return;

            int attack = GetAttack(whoHits);
            int defense = GetDefense(whoGetsHit);

            if (attack <= defense)
                return;

            whoGetsHit.Hero.Health -= (attack - defense);
            whoHits.Points -= DamageActionPoint;
        }

        private int GetAttack(Player player)
        {
            int rnd = new Random().Next(1, 5);
            return player.Hero.Force * rnd + player.Hero.Weapon.Damage;
        }

        private int GetDefense(Player player)
        {
            int protection = 0;
            if (player.Hero.Weapon.Discriminator.Equals(nameof(ShieldAndWeaponDto)))
            {
                var weapon = (ShieldAndWeaponDto)player.Hero.Weapon;
                protection += weapon.BodyProtection;
            }

            protection += player.Hero.Armor.BodyProtection;
            protection += player.Hero.Helmet.HeadProtection;
            protection += player.Hero.Boots.LegProtection;
            protection += player.Hero.Gauntlets.HandProtection;

            return protection;
        }
    }
}
