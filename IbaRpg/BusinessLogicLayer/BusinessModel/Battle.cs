﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.BusinessModel
{
    public class Battle
    {
        private bool firstPlayerTurn;

        public string Id { get; set; }
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }
        public BattleField Field { get; set; }
        public DamageCalculator Calculator { get; set; }

        public Battle() { }

        public Battle(Player player1, Player player2)
        {
            Player1 = player1;
            Player2 = player2;

            Calculator = new DamageCalculator();

            if (new Random().Next(1, 5000) % 2 == 0)
            {
                Player1.Turn = true;
                Player2.Turn = false;
            }
            else
            {
                Player1.Turn = false;
                Player2.Turn = true;
            }

            firstPlayerTurn = Player1.Turn ? true : false;
        }

        public Player WhoseTurn
        {
            get
            {
                return (firstPlayerTurn) ?
                    this.Player1 :
                    this.Player2;
            }
        }

        public bool IsOver
        {
            get
            {
                return !Player1.IsAlive || !Player2.IsAlive;
            }
        }

        public bool MakeTurn(int x, int y)
        {
            if (Field.Field[x][y] != 0)
                return false;

            if (firstPlayerTurn)
                return Turn(Player1, x, y);

            return Turn(Player2, x, y);

        }

        public void MakeHit()
        {
            int player1XY = Player1.Coordinate.X + Player1.Coordinate.Y;
            int player2XY = Player2.Coordinate.X + Player2.Coordinate.Y;

            int difference = Math.Abs(player1XY - player2XY);

            if (difference > 1)
                return;

            if (firstPlayerTurn)
            {
                Calculator.CalculateDamage(Player1, Player2);
                if (Player1.Points <= 0)
                    EndOfTurn();
            }
            else
            {
                Calculator.CalculateDamage(Player2, Player1);
                if (Player2.Points <= 0)
                    EndOfTurn();
            }
        }

        public void EndOfTurn()
        {
            if(firstPlayerTurn)
                Player1.Points = Player1.Hero.MovePoints;
            else
                Player2.Points = Player2.Hero.MovePoints;

            firstPlayerTurn = !firstPlayerTurn;
        }

        public List<Coordinate> GetAvailableTurns(Player player)
        {
            List<Coordinate> сoordinates = new List<Coordinate>();

            for (int i = 0; i < Field.Field.GetLength(0); i++)
            {
                for (int j = 0; j < Field.Field.GetLength(1); j++)
                {
                    int from = player.Coordinate.X + player.Coordinate.Y;
                    int to = i + j;

                    if (Math.Abs(to - from) <= player.Points)
                    {
                        if (Field.Field[i][ j] == 0)
                            сoordinates.Add(new Coordinate() { Y = i, X = j });
                    }
                }
            }

            return сoordinates;
        }

        private bool Turn(Player player, int x, int y)
        {
            var from = player.Coordinate.X + player.Coordinate.Y;
            var to = x + y;

            if (Math.Abs(to - from) > player.Hero.MovePoints)
                return false;

            Field.Field[player.Coordinate.Y][player.Coordinate.X] = 0;
            Field.Field[y][x] = 1;

            player.Coordinate.X = x;
            player.Coordinate.Y = y;

            player.Points -= Math.Abs(to - from);

            if (player.Points == 0)
            {
                firstPlayerTurn = !firstPlayerTurn;
                player.Points = player.Hero.MovePoints;
            }

            return true;
        }
    }
}
