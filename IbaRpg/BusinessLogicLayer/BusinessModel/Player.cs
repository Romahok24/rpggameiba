﻿using BusinessLogicLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.BusinessModel
{
    public class Player
    {
        public string Id { get; set; }
        public string GameId { get; set; }
        public bool Looking { get; set; }
        public Coordinate Coordinate { get; set; } = new Coordinate();
        public HeroDto Hero { get; set; }
        public int Points { get; set; }
        public int Health { get; set; }

        public string ImgPath { get; set; }

        public bool IsAlive => Health > 0;

        public bool Turn { get; set; }

        public Player() { }

        public Player(HeroDto hero)
        {
            Hero = hero;
            Points = Hero.MovePoints;
            Health = Hero.Health;
            ImgPath = $"{Hero.Race.Name}.png";
        }

        public override bool Equals(object obj)
        {
            Player other = obj as Player;

            if (other == null)
            {
                return false;
            }

            return this.Id.Equals(other.Id) && this.Hero.Name.Equals(other.Hero.Name);
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode() * this.Hero.Name.GetHashCode();
        }
    }
}
