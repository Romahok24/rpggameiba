﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.BusinessModel
{
    public class Coordinate
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Coordinate() { }
    }
}
