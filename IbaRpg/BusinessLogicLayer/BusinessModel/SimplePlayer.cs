﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.BusinessModel
{
    public class SimplePlayer
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Coordinate Coordinate { get; set; } = new Coordinate();
        public string ImgPath { get; set; }
        public int Health { get; set; }
    }
}
