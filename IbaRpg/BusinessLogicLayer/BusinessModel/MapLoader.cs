﻿using BusinessLogicLayer.DTO;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.BusinessModel
{
    public class MapLoader
    {
        public static IEnumerable<MapDto> LoadMap(IUnitOfWork db, string path)
        {
            int n = 0;            
            string[] mapTitles = new string[600];
            List<MapDto> map = new List<MapDto>();

            using (StreamReader sr = new StreamReader("testmap.txt"))
            {
                mapTitles = sr.ReadToEnd().Split(new char[] { '\n', ','}, StringSplitOptions.RemoveEmptyEntries);
            }

            var npc = db.BaseNPCs.Get().ToList();
            for (int i = 0; i < 20; i++)
            {
                for(int j = 0; j < 30; j++)
                {
                    bool isClickable = npc.Any(o => o.Name.Equals(mapTitles[n]));
                    map.Add(new MapDto { X = i + 1, Y = j + 1, Name = $"{mapTitles[n++]}.png", IsClicable = isClickable });
                }
            }

            return map;
        }
    }
}
