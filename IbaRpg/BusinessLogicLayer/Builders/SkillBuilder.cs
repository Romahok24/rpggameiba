﻿using BusinessLogicLayer.DTO;
using DataAccessLayer.Enums;

namespace BusinessLogicLayer.Builders
{
    public class SkillBuilder
    {
        /// <summary>
        /// Get start skills by race.
        /// </summary>
        /// <param name="race"></param>
        /// <returns></returns>
        public static void GetStartStats(HeroDto hero, Race race)
        {
            switch (race)
            {
                case Race.Lich: GetLichSkills(hero); break;
                case Race.Zombie: GetZombieSkills(hero); break;
                case Race.Vampire: GetVampireSkills(hero); break;
                case Race.SnowElf: GetSnowElfSkills(hero); break;
                case Race.HighElf: GetHighElfSkills(hero); break;
                case Race.DarkElf: GetDarkElfSkills(hero); break;
                case Race.Goblin: GetGoblinSkills(hero); break;
                case Race.Reptiloid: GetReptiloidSkills(hero); break;
                default: GetCatSkills(hero); break;
            }
        }

        private static void GetLichSkills(HeroDto hero)
        {
            hero.Health = 110;
            hero.Force = 5;
            hero.Intelligence = 7;
        }

        private static void GetZombieSkills(HeroDto hero)
        {
            hero.Force = 8;
            hero.Health = 140;
        }

        private static void GetVampireSkills(HeroDto hero)
        {
            hero.Health = 110;
            hero.Force = 5;
            hero.MovePoints = 7;
            hero.Agility = 6;
            hero.Intelligence = 5;
        }

        private static void GetSnowElfSkills(HeroDto hero)
        {
            hero.Health = 130;
            hero.Force = 7;
            hero.Intelligence = 5;
        }

        private static void GetHighElfSkills(HeroDto hero)
        {
            hero.Intelligence = 8;
        }

        private static void GetDarkElfSkills(HeroDto hero)
        {
            hero.Intelligence = 5;
            hero.Agility = 7;
            hero.MovePoints = 8;
        }

        private static void GetGoblinSkills(HeroDto hero)
        {
            hero.Force = 7;
            hero.Health = 130;
            hero.Agility = 5;
            hero.MovePoints = 6;
        }

        private static void GetReptiloidSkills(HeroDto hero)
        {
            hero.Agility = 5;
            hero.MovePoints = 6;
            hero.Intelligence = 7;
        }

        private static void GetCatSkills(HeroDto hero)
        {
            hero.Agility = 8;
            hero.MovePoints = 9;
        }
    }
}
