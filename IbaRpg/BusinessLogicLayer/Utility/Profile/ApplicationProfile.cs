﻿using BusinessLogicLayer.DTO;
using DataAccessLayer.Models;
using DataAccessLayer.Models.Heroes;
using DataAccessLayer.Models.Items;
using DataAccessLayer.Models.Items.Armors;
using DataAccessLayer.Models.Items.Weapons;
using DataAccessLayer.Models.Levels;
using DataAccessLayer.Models.NPC;
using DataAccessLayer.Models.Races;
using DataAccessLayer.Models.Users;

namespace BusinessLogicLayer.Utility.Profile
{
    public class ApplicationProfile : AutoMapper.Profile
    {
        public ApplicationProfile()
        {
            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<Hero, HeroDto>().ReverseMap();
            CreateMap<Armor, ArmorDto>().ReverseMap();
            CreateMap<BodyArmor, BodyArmorDto>().ReverseMap();
            CreateMap<BootsArmor, BootsArmorDto>().ReverseMap();
            CreateMap<GauntletsArmor, GauntletsArmorDto>().ReverseMap();
            CreateMap<HelmetArmor, HelmetArmorDto>().ReverseMap();
            CreateMap<Weapon, WeaponDto>().ReverseMap();
            CreateMap<MagicWeapon, MagicWeaponDto>().ReverseMap();
            CreateMap<MeleeWeapon, MeleeWeaponDto>().ReverseMap();
            CreateMap<RangeWeapon, RangeWeaponDto>().ReverseMap();
            CreateMap<ShieldAndWeapon, ShieldAndWeaponDto>().ReverseMap();
            CreateMap<SmallWeapon, SmallWeaponDto>().ReverseMap();
            CreateMap<TwoHandWeapon, TwoHandWeaponDto>().ReverseMap();
            CreateMap<PlayerAndItem, PlayerAndItemDto>().ReverseMap();
            CreateMap<Item, ItemDto>().ReverseMap();
            CreateMap<Level, LevelDto>().ReverseMap();
            CreateMap<HeroLevel, HeroLevelDto>().ReverseMap();
            CreateMap<ItemLevel, ItemLevelDto>().ReverseMap();
            CreateMap<BaseNPC, BaseNPCDto>().ReverseMap();
            CreateMap<EnemyNPC, EnemyNPCDto>().ReverseMap();
            CreateMap<FriendlyNPC, FriendlyNPCDto>().ReverseMap();
            CreateMap<Race, RaceDto>().ReverseMap();
            //CreateMap<Role, RoleDto>().ReverseMap();
            CreateMap<Location, LocationDto>().ReverseMap();
            CreateMap<Quest, QuestDto>().ReverseMap();
        }
    }
}
